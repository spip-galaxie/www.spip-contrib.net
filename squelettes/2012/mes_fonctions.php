<?php


// ajouter l'astérisque à tous les termes de recherche
define('_FULLTEXT_ASTERISQUE_PARTOUT', true);

/**
 * Critere {tout_voir} permet de deverouiller l'acces restreint sur une boucle
 *
 * @param unknown_type $idb
 * @param unknown_type $boucles
 * @param unknown_type $crit
 */
if (!function_exists('critere_tout_voir_dist')){
	function critere_tout_voir_dist($idb, &$boucles, $crit) {
		$boucle = &$boucles[$idb];
		$boucle->modificateur['tout_voir'] = true;
	}
}

// true quand on vient du wiki
// true dans l'espace prive
// true dans les crayons
// false dans les autres pages (publiques hors wiki)
function boucle_exclure_secteur() {
	if (!test_espace_prive() && !_request('action')) {
		include_spip('inc/filtres');
		$f = chercher_filtre('info_plugin');
		if($f('autorite','est_actif')) {
			return lire_config('autorite/espace_wiki');
		} else {
			return array(lire_config('gribouille/secteur_wiki'));
		}
	}
	return null;
}

//
// <BOUCLE(ARTICLES)> sans le wiki (secteur 607)
//
function boucle_ARTICLES($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$id_table = $boucle->id_table;
	$secteurs_wiki = join(',',boucle_exclure_secteur());

	if (!$boucle->modificateur['wiki'] && $secteurs_wiki) {
		$boucle->where[] = array(
			"'NOT IN'",
			"'$id_table.id_secteur'",
			"'($secteurs_wiki)'",
		);
	}

	return boucle_DEFAUT_dist($id_boucle, $boucles);
}

//
// <BOUCLE(RUBRIQUES)> sans le wiki (secteur 607)
//
function boucle_RUBRIQUES($id_boucle, &$boucles) {
	$boucle = &$boucles[$id_boucle];
	$id_table = $boucle->id_table;
	$secteurs_wiki = join(',',boucle_exclure_secteur());

	if (!$boucle->modificateur['wiki'] && $secteurs_wiki) {
		$boucle->where[] = array(
			"'NOT IN'",
			"'$id_table.id_secteur'",
			"'($secteurs_wiki)'",
		);
	}

	return boucle_DEFAUT_dist($id_boucle, $boucles);
}


function date_rfc822($date_heure) {
  list($annee, $mois, $jour) = recup_date($date_heure);
  list($heures, $minutes, $secondes) = recup_heure($date_heure);
  $time = mktime($heures, $minutes, $secondes, $mois, $jour, $annee);
  return gmdate("D, d M Y H:i:s +0100", $time);
}



function dec2hex($v) {
return substr('00'.dechex($v), -2);
}


function age_style($date) {
 	
// $decal en secondes
$decal = date("U") - date("U", strtotime($date));
			 
// 3 jours = vieux
$decal = min(1.0, sqrt($decal/(3*24*3600)));
			 
// Quand $decal = 0, c'est tout neuf : couleur vive
// Quand $decal = 1, c'est vieux : bleu pale
$red = ceil(128+127*(1-$decal));
$blue = ceil(130+60*$decal);
$green = ceil(200+55*(1-$decal));
										 
$couleur = dec2hex($red).dec2hex($green).dec2hex($blue);
											 
return 'background-color: #'.$couleur.';';
}

if (!test_espace_prive() AND !function_exists('generer_url_site')){
function generer_url_site($id){
	return generer_url_entite($id,'site');
}
}

function compter_visiteurs(){
	return count(preg_files(_DIR_TMP.'visites/','.'));
}


function ohloh_widgetize($bio,$id_auteur,$email){
	if (!$email OR !$id_auteur=intval($id_auteur)) return $bio;
	if (strpos($bio,"www.ohloh.net")!==FALSE) return $bio;
	$hash = md5($email);
	include_spip('inc/distant');
	$oh = recuperer_page("http://www.ohloh.net/accounts/$hash/widgets");
	if (!$oh
	 OR count($widgets = extraire_balises($oh,'textarea'))<5)
		$widget = "Pas de compte sur www.ohloh.net";
	else {
		$widget = preg_replace(',^<textarea[^>]*>,i','',$widgets[4]);
		$widget = preg_replace(',</textarea>$,i','',$widget);
		$widget = html_entity_decode($widget);
	}
	
	$bio .= $widget;
	// mettons a jour sauvagement !
	include_spip('abstract_sql');
	$bio_sql = sql_getfetsel('bio','spip_auteurs','id_auteur='.intval($id_auteur));
	sql_updateq('spip_auteurs',array('bio'=>$bio_sql."\n".$widget),'id_auteur='.intval($id_auteur));
	return $bio;
}
/*
function critere_compteur($idb, &$boucles, $crit){
	$boucle = &$boucles[$idb];
	
	$params = $crit->param;
	$table = array_shift($params);
	$table = $table[0]->texte;
	if(preg_match(',^(\w+)([<>=])([0-9]+)$,',$table,$r)){
		$table=$r[1];
		$op=$r[2];
		$op_val=$r[3];
	}
	$type = objet_type($table);
	$type_id = id_table_objet($type);
	$table_sql = table_objet_sql($type);
	
	$trouver_table = charger_fonction('trouver_table','base');
	$arrivee = array($table, $trouver_table($table, $boucle->sql_serveur));
	$depart = array($boucle->id_table,$trouver_table($boucle->id_table, $boucle->sql_serveur));

	if ($compt = calculer_jointure($boucle,$depart,$arrivee)){

		$boucle->select[]= "COUNT($compt.$type_id) AS compteur_$table";	
		if ($op)
			$boucle->having[]= array("'".$op."'", "'compteur_".$table."'",$op_val);
	}
}

function balise_COMPTEUR_dist($p) {
	$p->code = '';
	if (isset($p->param[0][1][0])
	AND $champ = ($p->param[0][1][0]->texte))
		return rindex_pile($p, "compteur_$champ", 'compteur');
  return $p;
}
*/

function sc_versions_compatibles($id,$type){
	$compatibles = array();
	
	static $versions = array(
	190=>'SPIP 1.9.0',
	191=>'SPIP 1.9.1',
	192=>'SPIP 1.9.2',
	200=>'SPIP 2.0.0',
	);
	static $versions_min_inc=array(
	12=>array(190,191,192,200), // toutes versions de spip...
	100=>array(190,191,192), // 1.9 toutes versions sans plus de precision...
	258=>array(190), // 1.9.0
	119=>array(191), // 1.9.1
	120=>array(192), // 1.9.2
	261=>array(200), // 2.0.0
	);
	static $versions_max_excl = array(
	260 => array(190,191,192,200),
	259 => array(200),
	);

	include_spip('base/abstract_sql');
	$id_objet = id_table_objet($type);
	$mot_min = array_map('reset',sql_allfetsel('id_mot',"spip_mots_liens","objet='$type' AND id_objet=".intval($id) . " AND " . sql_in('id_mot',array_keys($versions_min_inc))));
	foreach($mot_min as $mot)
		if (isset($versions_min_inc[$mot]))
			$compatibles = array_merge($compatibles,$versions_min_inc[$mot]);
	if (!count($compatibles))
		return "";
	$compatibles = array_unique($compatibles);

	$mot_max = array_map('reset',sql_allfetsel('id_mot',"spip_mots_liens","objet='$type' AND id_objet=".intval($id) . " AND " . sql_in('id_mot',array_keys($versions_max_excl))));
	foreach($mot_max as $mot)
		if (isset($versions_max_excl[$mot]))
			$compatibles = array_diff($compatibles,$versions_max_excl[$mot]);
	if (!count($compatibles))
		return "";

	$res = "";
	foreach ($compatibles as $v)
		$res .= "<li>".$versions[$v]."</li>";
	return $res;
}

/*
function filtre_date_dist($time,$format){return date($format,$time);}
*/

// Retire les liens en syntaxe Trac: [URL titre] ---> titre

function tracbrut($texte)
{
	if (preg_match_all('/[[][^ ]* *([^]]*)]/', $texte, $m, PREG_SET_ORDER))
		foreach($m as $r)
			$texte = str_replace($r[0], $r[1], $texte);
	return $texte ? $texte : 'Log';   
}

function slogan_aleatoire($titre,$lang=null){
	static $slogan = array();
	if (is_null($lang)){
		$lang = $GLOBALS['spip_lang'];
	}
	if (isset($slogan[$lang]))
		return $slogan[$lang];

	// si un fichier cache slogans_xx.txt existe, on l'utilise
	if ($f = find_in_path("slogans/slogans_$lang.txt")
			AND lire_fichier($f, $slogans)
			AND trim($slogans)
			AND ($n = count($slogans = explode("\n",$slogans)))>1){
		return $slogan[$lang] = str_replace("@slogan@",$titre,$slogans[rand(0, $n)]);
	}

	// sinon on se rabat sur la fonction
	if (!$slogan_aleatoire = charger_fonction("slogan_aleatoire_".$lang,"public",true)
		AND $lang!='fr'){
		// sinon on se rabat sur le slogan en francais
		return $slogan[$lang] = "<span lang='$lang'>".slogan_aleatoire($titre,'fr')."</span>";
	}
	return $slogan[$lang] = $slogan_aleatoire($titre);
}

function public_slogan_aleatoire_fr_dist($titre){
	define('_URL_SLOGAN_ALEATOIRE_FR',"http://hellday.free.fr/slogans/sloganfr.php?chaine=%s");
	$slogan = "";

	$url = str_replace("%s", urlencode($titre), _URL_SLOGAN_ALEATOIRE_FR);
	include_spip('inc/distant');
	if ($res = recuperer_page($url)){
		$res = extraire_balises($res,'body');
		$res = reset($res);
		$res = charset2unicode($res,"iso-8859-1");
		$res = preg_replace(",<style[^>]*>.*</style>,Uims","",$res);
		$res = preg_replace(",<h1[^>]*>.*</h1>,Uims","",$res);
		$res = preg_replace(",<form[^>]*>.*</form>,Uims","",$res);
		$slogan = trim(textebrut($res));
	}
	return $slogan;
}

function public_slogan_aleatoire_en_dist($titre){
	define('_URL_SLOGAN_ALEATOIRE_EN',"http://www.sloganizer.net/en/outbound.php?slogan=%s");
	$slogan = "";

	$url = str_replace("%s", urlencode($titre), _URL_SLOGAN_ALEATOIRE_EN);
	include_spip('inc/distant');
	if ($res = recuperer_page($url)){
		$slogan = trim(textebrut($res));
	}
	return $slogan;
}

function public_slogan_aleatoire_de_dist($titre){
	define('_URL_SLOGAN_ALEATOIRE_DE',"http://www.sloganizer.net/outbound.php?slogan=%s");
	$slogan = "";

	$url = str_replace("%s", urlencode($titre), _URL_SLOGAN_ALEATOIRE_DE);
	include_spip('inc/distant');
	if ($res = recuperer_page($url)){
		$slogan = trim(textebrut($res));
	}
	return $slogan;
}

function slogan_cache($lang,$fichier,$n=100){
	lire_fichier($fichier, $contenu);
	$slogans = explode("\n",trim($contenu));

	var_dump(count($slogans));

	$slogan_aleatoire = charger_fonction("slogan_aleatoire_$lang","public");
	while ($n-->0){
		$slogans[] = $slogan_aleatoire("@slogan@");
	}
	$slogans = array_unique($slogans);
	var_dump(count($slogans));
	ecrire_fichier($fichier, implode("\n",$slogans));
}


?>
