$(document).ready(function(){
	// bouton + menu sous 640px de largeur
	// on place le html du bouton
	$('#entetelogos').append(
		$('<button class="btn-nav burger" aria-label="afficher le menu de navigation"><span class="ham"></span><span class="ham"></span></button>')
	);
	// on actionne
	$('.btn-nav').bind('click',function(){
		$('#page').toggleClass('naviguer');
		$('#navigation').toggleClass('ouverte');
		$(this).toggleClass('btn-nav-ouvert');
	});
	// rehausser le btn-nav si scroll
	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
		var top_h = '20';
		if ( scroll > top_h ){
			$('.btn-nav').addClass('btn-nav-haut');
		}
		if ( scroll < top_h ){
			$('.btn-nav').removeClass('btn-nav-haut');
		}
	});
});