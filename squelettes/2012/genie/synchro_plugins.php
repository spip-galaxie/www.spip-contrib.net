<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function genie_synchro_plugins_dist($t){
	synchro_plugins(20);
	return 1;
}

function prefixes_from_jsons($jsons, $nb=null){
	$prefixes = array();

	foreach($jsons as $j)
		$prefixes = array_merge($prefixes,array_keys($j));

	$prefixes = array_unique($prefixes);
	$prefixes = array_filter($prefixes);

	// si nombre de plugins passe en arg, tirer au sort nb plugins a synchroniser
	if ($nb AND $nb<count($prefixes)){
		return array_rand(array_flip($prefixes), $nb);
	}
	return $prefixes;
}

function infos_from_jsons($prefixe, $jsons){
	$infos = array();

	foreach($jsons as $json){
		if (isset($json[$prefixe])){
			if (!$infos){
				$infos = $json[$prefixe];
			}
			else {
				foreach($json[$prefixe] as $v=>$i)
					if (!isset($infos[$v]) OR $i)
						$infos[$v] = $i;
			}
		}
	}
	ksort($infos);
	return $infos;
}

function synchro_plugins($nb = null){

	include_spip('inc/autoriser');

	$domains = array("http://www.spip-contrib.net/","http://contrib.spip.net/","https://contrib.spip.net/");
	$jsons = array();

	include_spip('inc/distant');
	foreach($domains as $d){
		$src = "https://plugins.spip.net/infos_plugin.api/?lien_doc=$d";
		$json = recuperer_page($src);
		$json = json_decode($json,true);
		if (isset($json['plugins'])
		  AND $json['plugins'])
			$jsons[] = $json['plugins'];
	}

	if (!count($jsons))
		return;


	$motsv = array(
		"SPIP-2.0" => 261,
		"SPIP-2.1" => 278,
		"SPIP-3.0" => 285,
		"SPIP-3.1" => 291,
		"SPIP-3.2" => 292
	);
	$motsc = array(
		"squelette" => 134,
		"theme" => 206,
		"plugin" => 112,
	);
	$motsall = array_values(array_merge($motsc, $motsv));


	include_spip('action/editer_liens');
	include_spip('action/editer_article');

	$prefixes = prefixes_from_jsons($jsons, $nb);

	foreach($prefixes as $prefixe){
		$infos = infos_from_jsons($prefixe,$jsons);

		spip_log("---- Synchro $prefixe","synchroplugins"._LOG_INFO_IMPORTANTE);
		$liens_mots_art = array();
		$liens_mots_doc = array();
		$liens_docs = array();

		// balayer les versions
		foreach($infos as $vspip=>$d){
			if ($d){
				if ($id_article = get_article_from_url($d['lien_doc'])){
					if (isset($motsv[$vspip]))
						$liens_mots_art[$id_article][] = $motsv[$vspip];
					if (isset($motsc[$d['categorie']]))
						$liens_mots_art[$id_article][] = $motsc[$d['categorie']];
					else
						$liens_mots_art[$id_article][] = $motsc['plugin'];

					// verifier le zip
					if ($id_document = get_document_from_url($d['archive'],"Version ".$d['version'])){
						$liens_docs[$id_article][] = $id_document;
						if (isset($motsv[$vspip]))
							$liens_mots_doc[$id_document][] = $motsv[$vspip];
					}

					// verfier le champ url_site
					$url_p = "https://plugins.spip.net/".strtolower($prefixe);
					$url_site = sql_getfetsel('url_site','spip_articles','id_article='.intval($id_article));
					if (!$url_site){
						spip_log("$prefixe: Modifier url_site=$url_p sur article $id_article","synchroplugins"._LOG_INFO_IMPORTANTE);
						autoriser_exception("modifier","article",$id_article);
						article_modifier($id_article,array('url_site'=>$url_p));
						autoriser_exception("modifier","article",$id_article,false);
					}
					elseif($url_site!=$url_p){
						spip_log("$prefixe: url_site incorrecte sur article $id_article","synchroplugins"._LOG_INFO_IMPORTANTE);
					}
				}
				else {
					spip_log("$prefixe: URL ".$d['lien_doc']." non trouvee","synchroplugins"._LOG_INFO_IMPORTANTE);
				}
			}
		}

		update_liens_mots($liens_mots_art, 'article', $motsall);
		update_liens_mots($liens_mots_doc, 'document', $motsall);
		foreach($liens_mots_art as $id_article => $dummy){
			update_liens_trads($id_article, array('mot' => $motsall));
		}

		// lier les docs aux articles
		foreach($liens_docs as $id_article => $id_docs){
			objet_associer(array('document'=>$id_docs),array('article'=>$id_article));
			update_liens_trads($id_article,array('document'=>$id_docs));
		}

	}

}

include_spip('inc/urls');
function get_article_from_url($url){
	static $cache = array();

	if (isset($cache[$url])) return $cache[$url];

	if (preg_match(",^http://(?:www\.|spip-)?(?:spip-)?contrib(?:\.spip)?\.net/(\d+)$,",$url,$m)){
	spip_log(__LINE__  ,"synchroplugins"._LOG_INFO_IMPORTANTE);
		return $cache[$url] = intval($m[1]);
	}

	//var_dump(urls_decoder_url($url));
	list($fond,$contexte,$url_redirect) = urls_decoder_url($url);
	spip_log(__LINE__ ."$fond $contexte $url_redirect " ,"synchroplugins"._LOG_INFO_IMPORTANTE);
	if ($fond=='rubrique' AND $id_rubrique=$contexte['id_rubrique']){
		// chercher si un seul article
	spip_log(__LINE__  ,"synchroplugins"._LOG_INFO_IMPORTANTE);
		$a = sql_allfetsel('id_article','spip_articles','id_rubrique='.intval($id_rubrique)." AND statut='publie'");
		if (count($a)==1){
	spip_log(__LINE__  ,"synchroplugins"._LOG_INFO_IMPORTANTE);
			$a = reset($a);
			spip_log("$url => ".reset($a),"synchroplugins"._LOG_INFO_IMPORTANTE);
			return $cache[$url] = reset($a);
		}
	}
	if ($fond!=="article") return $cache[$url]=false;
	if (!isset($contexte['id_article'])) return $cache[$url]=false;
	#spip_log($contexte,"synchroplugins"._LOG_INFO_IMPORTANTE);
	spip_log("$url => ".$contexte['id_article'],"synchroplugins"._LOG_INFO_IMPORTANTE);
	return $cache[$url]=$contexte['id_article'];
}

function get_document_from_url($url_zip, $titre){
	static $cache = array();
	$id_document = 0;

	include_spip('action/editer_document');
	$ajouter_documents = charger_fonction('ajouter_documents','action');
	
	$row = sql_fetsel('*','spip_documents',"distant='oui' AND fichier=".sql_quote($url_zip));
	if (!$row){
		$file = array(
			'tmp_name' => $url_zip,
			'name' => '',
			'mode' => 'document',
			'distant' => 'oui',
		);
		$ids = $ajouter_documents('new',array($file));
		$id_document= reset($ids);
		spip_log("Ajout document distant $url_zip : #$id_document","synchroplugins"._LOG_INFO_IMPORTANTE);

		if (!$id_document)
			return false;

		$row = sql_fetsel('*','spip_documents',"id_document=".intval($id_document));
	}
	if (!$id_document = $row['id_document'])
		return false;

	// verifier le titre
	if ($row['titre']!==$titre){
		spip_log("Titre document #id_document : $titre ","synchroplugins"._LOG_INFO_IMPORTANTE);
		autoriser_exception("modifier","document",$id_document);
		document_modifier($id_document,array('titre' => $titre));
		autoriser_exception("modifier","document",$id_document,false);
	}

	return $id_document;
}

function update_liens_mots($liens_mots, $objet, $motsall){

		// mettre a jour les liens
		// ne pas retirer compat 2.0 et 2.1 car plugin.spip pas exhaustif
		// sauf si compat 2.0 ou 2.1 connue
		$motsv_unknown = array_diff(array(261, 278), array_merge($liens_mots));
		foreach($liens_mots as $id_objet=>$mots){
			$set = array_unique($mots);
			$remove = array_diff($motsall,$set);
			// les liens existants
			$links = objet_trouver_liens(array('mot'=>$motsall),array($objet=>$id_objet));
			$l = array();
			foreach($links as $link) $l[] = $link['id_mot'];

			$remove = array_intersect($remove,$l);
			$remove = array_diff($remove, $motsv_unknown);
			if (count($remove)){
				objet_dissocier(array('mot'=>$remove),array($objet=>$id_objet));
				spip_log("$objet $id_objet remove ".implode(",",$remove),"synchroplugins"._LOG_INFO_IMPORTANTE);
			}
			$set = array_diff($set,$l);
			if (count($set)){
				objet_associer(array('mot'=>$set),array($objet=>$id_objet));
				spip_log("$objet $id_objet set ".implode(",",$set),"synchroplugins"._LOG_INFO_IMPORTANTE);
			}
		}
}

function update_liens_trads($id_article,$quoi){

	list($objet,) = each($quoi);
	$set = array();
	$dup = objet_trouver_liens($quoi,array('article'=>$id_article));
	foreach($dup as $l) $set[] = $l[$objet];
	$set = array_unique($set);

	$trads = sql_allfetsel('id_article','spip_articles','id_trad='.intval($id_article).' AND id_article<>'.intval($id_article));
	foreach($trads as $t){
		$id_trad = $t['id_article'];
		$exs = objet_trouver_liens($quoi,array('article'=>$id_trad));
		$l = array();
		foreach($exs as $ex) $l[] = $ex[$objet];

		$remove = array_diff($l,$set);
		$set = array_diff($set,$l);
		if (count($remove)){
			objet_dissocier(array($objet=>$remove),array('article'=>$id_trad));
			spip_log("TRAD article $id_trad remove $objet ".implode(",",$remove),"synchroplugins"._LOG_INFO_IMPORTANTE);
		}
		if (count($set)){
			objet_associer(array($objet=>$set),array('article'=>$id_trad));
			spip_log("TRAD article $id_trad set $objet ".implode(",",$set),"synchroplugins"._LOG_INFO_IMPORTANTE);
		}
	}
}
