<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Bare de nav
'visiteurs_en_ce_moment' => '<strong>@nb@</strong> visiteurs en ce moment',
'contribs' => 'contribs',
'documentation' => 'Documentation',
'glossaire' => 'Glossaire',
'contribuer' => 'Contribuer',
'aide' => 'Aide',
'telechargement' => 'T&eacute;l&eacute;chargements',
'wiki' => 'Carnet wiki',

'info_rechercher_02' => 'Rechercher sur ce site',
'info_rechercher' => 'Trouver !',
'info_auteurs' => 'Contributeurs',

// Information sur l'article
'versions'=>'Compatibilit&eacute;',
'version_inconnue'=>'<em>Compatibilit&eacute; inconnue&nbsp;!</em>',

// Menus lateraux
'dans_autres_langues' => 'Dans les autres langues',
'c_est_chaud' => '&Ccedil;a br&ucirc;le !',
'derniers_articles' => 'Les derni&egrave;res',
'articles_top_notes' => 'Vos pr&eacute;f&eacute;r&eacute;es',
'articles_top_popularite' => 'Les plus lues',

// Infos auteur
'login_login2' => 'Login&nbsp;:',
'ma_page' => 'Ma page',
'participation_auteur'=>'a particip&eacute; &agrave;&nbsp;:',

// pied
'ca_discute_par_ici' => '&Ccedil;a discute par ici',
'ca_spip_par_la' => '&Ccedil;a spipe par l&agrave;',


//
// Special pour spip.net
//

// Barre de navigation de spip.net :

// autre...
'date_maj' => 'Dernière modification de cette page le ',
'dernieres_modifs' => 'Dernières modifications',
'maj' => 'maj', // abbreviation de 'mise a jour'
'sites_realises_avec_spip' => 'Sites r&eacute;alis&eacute;s avec SPIP',
'derniers_sites_realises_avec_spip' => 'Derniers sites r&eacute;alis&eacute;s avec SPIP',
'sites_references' => 'Sites r&eacute;f&eacute;renc&eacute;s',

//
// Des trucs qui manquent, quelque soit le site...
//

// pour la navigation :
'accueil' => 'Accueil',
'lire_suite' => 'Lire la suite',
'liens_utiles' => 'Liens utiles',
'lien_direct_forum' => 'Aller au forum',
'retour_top' => 'Retour en haut de la page',

// autres...
'quoideneuf' => 'Quoi de neuf ?',
'FAQ' => 'FAQ',

// specifique SPIP :
'squelette' => 'Squelette',
'squelettes' => 'Squelettes',
'squelette_voir' => 'Voir le squelette de cette page',
'squelettes_dossier' => 'Dossier squelettes',
's_inscrire' => 'Je veux contribuer !',
'pass_vousinscrire' => 'Je m\'inscris sur Spip-Contrib',
'sur_le_carnet' => 'Sur le Carnet Wiki',

// messagerie :
'messages_recus' => 'Messages re&ccedil;us',
'messages_envoyes' => 'Messages envoy&eacute;s',
'ecrire_message' => 'Envoyer un message',

// les tris :
'par_pertinence' => 'Les plus pertinents',
'par_date' => 'Les derniers publi&eacute;s',
'par_popularite' => 'Les plus populaires',
'par_note' => 'Les mieux not&eacute;s',
'par_nom' => 'Par Nom',
'par_contributions' => 'Les contributeurs principaux',
'par_titre' => 'Par Titre',

'ok' => 'ok',
);

?>
