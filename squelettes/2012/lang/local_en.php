<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Bare de nav
'visiteurs_en_ce_moment' => '<strong>@nb@</strong> visitors now',
'contribs' => 'contribs',
'documentation' => 'Documentation',
'glossaire' => 'Glossary',
'contribuer' => 'Contribute',
'aide' => 'Help',
'telechargement' => 'Download',
'wiki' => 'Wiki notepad',

'info_rechercher_02' => 'Search in this site',
'info_rechercher' => 'Found!',
'info_auteurs' => 'Contributors',

// Information sur l'article
'versions'=>'Compatibility',
'version_inconnue'=>'<em>Unknown Compatibility!</em>',
// Menus lateraux
'dans_autres_langues' => 'In other languages',
'c_est_chaud' => 'That\'s hot!',
'derniers_articles' => 'Keep going!',
'articles_top_notes' => 'Most rated',
'articles_top_popularite' => 'Most read',

// Infos auteur
'login_login2' => 'Login:',
'ma_page' => 'My page',
'participation_auteur'=>'contribute to:',

// pied
'ca_discute_par_ici' => 'Here they\'re talking',
'ca_spip_par_la' => 'There they\'re spiping',

//
// Special pour spip.net
//

// Barre de navigation de spip.net :

// autre...
'date_maj' => 'updated on',
'dernieres_modifs' => 'Latest modifications',
'maj' => 'update',
'sites_realises_avec_spip' => 'SPIP inside',
'derniers_sites_realises_avec_spip' => 'Latest sites designed by SPIP',
'sites_references' => 'Referenced sites',

//
// Des trucs qui manquent, quelque soit le site...
//

// pour la navigation :

'accueil' => 'Home',
'lire_suite' => 'Read more',
'liens_utiles' => 'Useful links',
'lien_direct_forum' => 'Go to the forum',
'retour_top' => 'Back to top',

// autres...
'quoideneuf' => 'What\'s new?',
'FAQ' => 'FAQ',

// specifique SPIP :
'squelette' => 'Template',
'squelettes' => 'Templates',
'squelette_voir' => 'Show the template of this page',
'squelettes_dossier' => 'Templates folder',
's_inscrire' => 'I want to contribute!',
'pass_vousinscrire' => 'Register with Spip-Contrib',
'sur_le_carnet' => 'In the Wiki',

// messagerie :
'messages_recus' => 'Messages received',
'messages_envoyes' => 'Messages sent',
'ecrire_message' => 'Send a message',

// les tris :
'par_pertinence' => 'Most relevant',
'par_date' => 'Last published',
'par_popularite' => 'Most popular',
'par_note' => 'Most rated',
'par_nom' => 'By Name',
'par_contributions' => 'Main contributors',
'par_titre' => 'By title'

);

?>
