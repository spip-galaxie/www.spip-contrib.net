<?php
define('_BOUCLE_PROFILER', 5000);
define('_CALCUL_PROFILER', 100);

if (strncmp(_request('recherche'),'http',4)==0 and _request('page')!='recherche'){
	$ecran_securite_raison = 'Recherche mal formee';
	if ($GLOBALS['ip'] AND date('s')==0) {
		touch(_DIR_RACINE . _NOM_TEMPORAIRES_INACCESSIBLES . 'flood/' . $GLOBALS['ip']);
	}
}
if (isset($ecran_securite_raison)) {
        header("HTTP/1.0 403 Forbidden");
        header("Expires: Wed, 11 Jan 1984 05:00:00 GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-Type: text/html");
        die("<html><title>Error 403: Forbidden</title><body><h1>Error 403</h1><p>You are not authorized to view this page ($ecran_securite_raison)</p></body></html>");
}

//if (is_dir($f='/dev/shm/cache-contribspipnet')) define('_DIR_CACHE',"$f/");
define('_UNIVERS_STATSV_FILE','../IMG/spip-histoversion-stats-192030.json');

// ajouter un bloc more/ qui contient les forums
$GLOBALS['z_blocs']=array('contenu','navigation','extra','head','more');
// activer le chargement parallele sur les blocs contenu et more
define('_Z_AJAX_PARALLEL_LOAD','navigation,more');

// Quelques reglages d'affichage
#$GLOBALS['table_des_traitements']['TITRE'][] = 'typo(supprimer_numero(%s))';
// $GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
// $GLOBALS['fin_intertitre'] = "</h2>\n";
$GLOBALS['puce'] = '- ';


// si c'est une page forum ou une vieille page : 
// un GET est redirige sur l'article de l'id_article ou sur la home
// un POST est refuse en 403
if ($p=_request('page') AND in_array($p,array('forum'))){
        if ($_SERVER["REQUEST_METHOD"]=="POST") {
                $raison = "Acces interdit";
                header("HTTP/1.0 403 Forbidden");
                header("Expires: Wed, 11 Jan 1984 05:00:00 GMT");
                header("Cache-Control: no-cache, must-revalidate");
                header("Pragma: no-cache");
                header("Content-Type: text/html");
                die("<html><title>Error 403: Forbidden</title><body><h1>Error 403</h1><p>You are not authorized to view this page ($raison)</p></body></html>");
        }
        else {
                $url = "https://contrib.spip.net/";
                if ($id_article = intval(_request('id_article'))){
                        $url .= "?article$id_article";
                }
                include_spip('inc/headers');
		spip_initialisation_core(
			(_DIR_RACINE . _NOM_PERMANENTS_INACCESSIBLES),
			(_DIR_RACINE . _NOM_PERMANENTS_ACCESSIBLES),
			(_DIR_RACINE . _NOM_TEMPORAIRES_INACCESSIBLES),
			(_DIR_RACINE . _NOM_TEMPORAIRES_ACCESSIBLES)
		);
                redirige_par_entete($url, '', 301);
        }
}


# economiser du cache en n'acceptant qu'un HTTP_HOST
#if ($_SERVER['REQUEST_METHOD'] == 'GET'
#AND $_SERVER['HTTP_HOST'] == 'contrib.spip.net') {
#	@header('Location: https://contrib.spip.net'.$_SERVER['REQUEST_URI']);
#}

if ($_SERVER['HTTP_HOST'] == 'files.spip.org'){
#var_dump($_SERVER['PHP_SELF']);
#var_dump($_SERVER['REQUEST_URI']);
        $GLOBALS['dossier_squelettes'] = 'squelettes/files.spip.org:'.$GLOBALS['dossier_squelettes'];
        $GLOBALS['marqueur'].='files:';
}

//
// *** Parametrage par defaut de SPIP ***
//
// Ces parametres d'ordre technique peuvent etre modifies
// dans ecrire/mes_options (_FILE_OPTIONS) Les valeurs
// specifiees dans ce dernier fichier remplaceront automatiquement
// les valeurs ci-dessous.
//
// Pour creer ecrire/mes_options : recopier simplement
// les lignes ci-dessous, et ajouter le marquage de debut et
// de fin de fichier PHP ("< ?php" et "? >", sans les espaces)
//

// Prefixe et chemin des cookies
// (a modifier pour installer des sites SPIP dans des sous-repertoires)
$cookie_prefix = "contrib";
$cookie_path = "";

// Type d'URLs
// 'page': spip.php?article123 [c'est la valeur par defaut pour SPIP 1.9]
// 'html': article123.html
// 'propres': Titre-de-l-article <http://lab.spip.net/spikini/UrlsPropres>
// 'propres2' : Titre-de-l-article.html (base sur 'propres')
// 'standard': article.php3?id_article=123 [urls SPIP < 1.9]
$type_urls = 'propres';

// Quota : la variable $quota_cache, si elle est > 0, indique la taille
// totale maximale desiree des fichiers contenus dans le CACHE/ ;
// ce quota n'est pas "dur", il ne s'applique qu'une fois par heure et
// fait redescendre le cache a la taille voulue ; valeur en Mo
// Si la variable vaut 0 aucun quota ne s'applique
$quota_cache = 150;


// 	*** Fin du paramtrage ***

// {doublons} ou {unique}
// attention: boucle->doublons designe une variable qu'on affecte
function critere_doublons_trad_dist($idb, &$boucles, $crit) {
	$boucle = &$boucles[$idb];
	$nom = !isset($crit->param[0]) ? "''" : calculer_liste($crit->param[0], array(), $boucles, $boucles[$idb]->id_parent);
	// mettre un tableau pour que ce ne soit pas vu comme une constante
	$boucle->where[]= array("sql_in('".$boucle->id_table . '.id_trad' .
	  "', " .
	  '"-1".$doublons[' .
	  "('" .
	  $boucle->type_requete . 
	  "'" .
	  ($nom == "''" ? '' : " . $nom") .
	  ')], \'' . 
	  ($crit->not ? '' : 'NOT') .
				"')");
}


$GLOBALS['spip_pipeline']['taches_generales_cron'] .= "|spipcont_taches_generales_cron";
function spipcont_taches_generales_cron($taches_generales){
	$taches_generales['synchro_plugins'] = 3000;
	return $taches_generales;
}

###################
# Gestion du wiki #
###################
# avertit qu'on va retirer les articles de ce secteur des boucles standard
define('SECTEURS_WIKI', '607');
# la suite est geree par le plugin "Autorite"

###################
# parametrages pour "Autorité" #
###################
# defini les "webmestres" au sens de Autorite
define('_ID_WEBMESTRES', '1:4:198:589:5384:6809:2650:5645');

##################
# parametrage pour "urls_libres" #
###################
# en association avec le htaccess adequat (cf les notes du plugin xml)
# choix de l'oprion sans le "?"
define ('_debut_urls_propres', '') ;
define('_MARQUEUR_URL', '');
define('_URLS_PROPRES_MAX', 55);


if ($i=_request('debut_comments-list')
	  AND strncmp($i,'@@',2)==0
		AND $id_forum = intval(substr($i,2))){


	@spip_initialisation_core(
		(_DIR_RACINE  . _NOM_PERMANENTS_INACCESSIBLES),
		(_DIR_RACINE  . _NOM_PERMANENTS_ACCESSIBLES),
		(_DIR_RACINE  . _NOM_TEMPORAIRES_INACCESSIBLES),
		(_DIR_RACINE  . _NOM_TEMPORAIRES_ACCESSIBLES)
	);

	include_spip('base/abstract_sql');
	if ($id_thread = sql_getfetsel('id_thread', "spip_forum", "id_forum=".intval($id_forum)." AND statut='publie'"))
		// on place la pagination indirecte sur le thread, au lieu du forum
		set_request('debut_comments-list',"@$id_thread");
	else
		// sinon au debut de la liste paginee
		set_request('debut_comments-list',"0");
}

?>
