<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Bare de nav
'visiteurs_en_ce_moment' => '@nb@ visitors now',
'documentation' => 'Documentation',
'glossaire' => 'Glossary',
'contribuer' => 'Contribute',
'aide' => 'Help',
'telechargement' => 'Download',
'wiki' => 'Wiki notepad',

'login_login2' => 'Login:',
'ma_page' => 'My page',

'derniers_articles' => 'Keep going!',
'c_est_chaud' => 'That\'s hot!',

// pied
'ca_discute_par_ici' => 'Here they\'re talking',
'ca_spip_par_la' => 'There they\'re spiping',

// autre...
'dernieres_modifs' => 'Latest modifications',
'maj' => 'upgrade', // abbreviation de 'mise a jour'
'sites_realises_avec_spip' => 'SPIP inside',
'derniers_sites_realises_avec_spip' => 'Latest sites designed by SPIP',
'sites_references' => 'Referenced sites',

//
// Des trucs qui manquent, quelque soit le site...
//

// pour la navigation :
'accueil' => 'Home',
'lire_suite' => 'Read more',
'liens_utiles' => 'Useful links',
'lien_direct_forum' => 'Go to the forum',
'retour_top' => 'Back to top',

// autres...
'quoideneuf' => 'What\'s new?',
'FAQ' => 'FAQ',

// specifique SPIP :
'squelette' => 'Template',
'squelettes' => 'Templates',
'squelette_voir' => 'Show the template of this page',
'squelettes_dossier' => 'Templates folder',
's_inscrire' => 'Register',

// Voir aussi : http://fraichdist.online.fr/spip.php?article292

);

?>
