Ce dossier contenait historiquement les fichiers mes_options.php de contrib + le genie de synchronisation des infos avec plugins.spip.net (pour avoir automatiquement les infos de compatibilité).

C'est désormais déplacé dans le plugin unique de squelettes de contrib

https://git.nursit.net/spip/galactic/contrib/
https://git.nursit.net/spip/galactic/contrib/commit/aceab97e14a2e7e08751b38d14f2800c432ded55
